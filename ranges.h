#pragma once

bool
check_rgb_range (double r, double g, double b)
{
  bool correct = true;
  correct &= r >= 0.0 && r <= 255.0;
  correct &= g >= 0.0 && g <= 255.0;
  correct &= b >= 0.0 && b <= 255.0;
  return correct;
}

bool
check_one_zero_range (double a, double b, double c)
{
  bool correct = true;
  correct &= a >= 0.0 && a <= 1.0;
  correct &= b >= 0.0 && b <= 1.0;
  correct &= c >= 0.0 && c <= 1.0;
  return correct;
}

bool
check_hsv_range (double h, double s, double v)
{
  bool correct = true;
  correct &= h >= 0.0 && h <= 360.0;
  correct &= s >= 0.0 && s <=   1.0;
  correct &= v >= 0.0 && v <=   1.0;
  return correct;
}

bool
check_XYZ_range (double X, double Y, double Z)
{
  bool correct = true;
  correct &= X >= 0.0 && X <= 100.0;
  correct &= Y >= 0.0 && Y <= 100.0;
  correct &= Z >= 0.0 && Z <= 100.0;
  return correct;
}

bool
check_cmyk_range (double c, double m, double y, double k)
{
  bool correct = true;
  correct &= check_one_zero_range(c, m, y);
  correct &= k >= 0.0 && k <= 1.0;
  return correct;
}

bool
check_laB_range (double l, double a, double B)
{
  bool correct = true;
  correct &= l >=    0.0 && l <=  100.0;
  correct &= a >= -128.0 && a <=  127.0;
  correct &= B >= -128.0 && B <=  127.0;
  return correct;
}

bool
check_LUV_range (double L, double U, double V)
{
  bool correct = true;
  correct &= L >=    0.0 && L <=  100.0;
  correct &= U >= -100.0 && U <=  100.0;
  correct &= V >= -100.0 && V <=  100.0;
  return correct;
}
