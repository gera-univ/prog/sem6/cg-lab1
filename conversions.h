#pragma once

#include <math.h>

#define EPS 10e-7
#define EQ(x, y) (x - y < EPS && x - y > -EPS)

// white color in XYZ
static double Xw = 95.047;
static double Yw = 100;
static double Zw = 108.883;

void
convert_rgb_to_cmyk (double r, double g, double b,
                    double *ret_c, double *ret_m, double *ret_y, double *ret_k)
{
  double r1 = r / 255.0;
  double g1 = g / 255.0;
  double b1 = b / 255.0;
  *ret_k = 1.0 - MAX (MAX (r1, g1), b1);
  *ret_c = (1.0 - r1 - *ret_k) / (1.0 - *ret_k);
  *ret_m = (1.0 - g1 - *ret_k) / (1.0 - *ret_k);
  *ret_y = (1.0 - b1 - *ret_k) / (1.0 - *ret_k);
}

void
convert_cmyk_to_rgb (double c, double m, double y, double k,
                     double *ret_r, double *ret_g, double *ret_b)
{
  double cyan = c;
  double magenta = m;
  double yellow = y;
  double key = k;
  *ret_r = 255.0 * (1 - cyan) * (1 - key);
  *ret_g = 255.0 * (1 - magenta) * (1 - key);
  *ret_b = 255.0 * (1 - yellow) * (1 - key);
}

void
convert_rgb_to_hsv (double r, double g, double b,
                    double *ret_h, double *ret_s, double *ret_v)
{
  double r1 = r / 255.0;
  double g1 = g / 255.0;
  double b1 = b / 255.0;
  double c_max = MAX (MAX (r1, g1), b1);
  double delta = c_max - MIN (MIN (r1, g1), b1);
  double hue = 0;
  if (EQ (c_max, r1))
   {
    hue = 60 * fmod ((g1 - b1) / delta, 6);
   }
  else if (EQ (c_max, g1))
   {
    hue = 60 * ((b1 - r1) / delta + 2);
   }
  else if (EQ (c_max, b1))
   {
    hue = 60 * ((r1 - g1) / delta + 4);
   }
  double saturation = 0;
  if (!EQ (c_max, 0))
   {
    saturation = delta / c_max;
   }
  *ret_h = hue;
  *ret_s = saturation;
  *ret_v = c_max;
}

void
convert_hsv_to_rgb (double h, double s, double v,
                    double *ret_r, double *ret_g, double *ret_b)
{
  double c = v * s;
  double x = c * (1 - fabs (fmod (h/60, 2) - 1));
  double m = v - c;
  double red, green, blue;
  if (h < 60)
   {
    red = c;
    green = x;
    blue = 0;
   }
  else if (h < 120)
   {
    red = x;
    green = c;
    blue = 0;
   }
  else if (h < 180)
   {
    red = 0;
    green = c;
    blue = x;
   }
  else if (h < 240)
   {
    red = 0;
    green = x;
    blue = c;
   }
  else if (h < 300)
   {
    red = x;
    green = 0;
    blue = c;
   }
  else if (h < 360)
   {
    red = c;
    green = 0;
    blue = x;
   }
  *ret_r = (red + m) * 255;
  *ret_g = (green + m) * 255;
  *ret_b = (blue + m) * 255;
}

static double
rgb_XYZ_F (double x)
{
  return x >= 0.04045 ? pow ((x+0.055)/1.055, 2.4) : x/12.92;
}

static double
XYZ_rgb_F (double x)
{
  return x >= 0.0031308 ? 1.055 * pow (x, 1/2.4) - 0.055 : 12.92 * x;
}

void
convert_rgb_to_XYZ (double r, double g, double b,
                    double *ret_X, double *ret_Y, double *ret_Z)
{
  double coeff[3][3] = {{0.412453, 0.357580, 0.180423},
                        {0.212671, 0.715160, 0.072169},
                        {0.019334, 0.119193, 0.950227}};
  double rn = rgb_XYZ_F (r/255) * 100;
  double gn = rgb_XYZ_F (g/255) * 100;
  double bn = rgb_XYZ_F (b/255) * 100;
  *ret_X = coeff[0][0] * rn + coeff[0][1] * gn + coeff[0][2] * bn;
  *ret_Y = coeff[1][0] * rn + coeff[1][1] * gn + coeff[1][2] * bn;
  *ret_Z = coeff[2][0] * rn + coeff[2][1] * gn + coeff[2][2] * bn;
}

void
convert_XYZ_to_rgb (double X, double Y, double Z,
                    double *ret_r, double *ret_g, double *ret_b)
{
  double coeff[3][3] = {{ 3.2406, -1.5372, -0.4986},
                        {-0.9689,  1.8758,  0.0415},
                        { 0.0557, -0.2040,  1.0570}};
  double rn = coeff[0][0] * X/100 + coeff[0][1] * Y/100 + coeff[0][2] * Z/100;
  double gn = coeff[1][0] * X/100 + coeff[1][1] * Y/100 + coeff[1][2] * Z/100;
  double bn = coeff[2][0] * X/100 + coeff[2][1] * Y/100 + coeff[2][2] * Z/100;
  *ret_r = XYZ_rgb_F(rn) * 255;
  *ret_g = XYZ_rgb_F(gn) * 255;
  *ret_b = XYZ_rgb_F(bn) * 255;
}

// alsa used for XYZ->LUV
static double 
XYZ_laB_F (double x)
{
  return x >= 0.008856 ? cbrt(x) : 7.787*x + 16/116;
}

// also used for LUV->XYZ
static double 
laB_XYZ_F (double x)
{
  return x*x*x >= 0.008856 ? x*x*x : (x-16/116)/7.787;
}

void
convert_XYZ_to_laB (double X, double Y, double Z,
                    double *ret_l, double *ret_a, double *ret_B)
{
  *ret_l = 116 *  XYZ_laB_F (Y/Yw) - 16;
  *ret_a = 500 * (XYZ_laB_F (X/Xw) - XYZ_laB_F (Y/Yw));
  *ret_B = 200 * (XYZ_laB_F (Y/Yw) - XYZ_laB_F (Z/Zw));
}

void
convert_laB_to_XYZ (double l, double a, double B,
                    double *ret_X, double *ret_Y, double *ret_Z)
{
  *ret_X = laB_XYZ_F ((l+16)/116) * Xw;
  *ret_Y = laB_XYZ_F (a/500 + (l+16)/116) * Yw;
  *ret_Z = laB_XYZ_F ((l+16)/116 - B/200) * Zw;
}

void
convert_XYZ_to_LUV (double X, double Y, double Z,
                    double *ret_L, double *ret_U, double *ret_V)
{
  double U_ = 4*X/(X+15*Y+3*Z);
  double V_ = 9*Y/(X+15*Y+3*Z);
  double Uw_ = 4*Xw*(Xw+15*Yw+3*Zw);
  double Vw_ = 9*Yw*(Xw+15*Yw+3*Zw);
  *ret_L = 116 * XYZ_laB_F(Y/Yw) - 16;
  *ret_U = 13*(*ret_L)*(U_-Uw_);
  *ret_V = 13*(*ret_L)*(V_-Vw_);  
}

void
convert_LUV_to_XYZ (double L, double U, double V,
                    double *ret_X, double *ret_Y, double *ret_Z)
{
  double Uw_ = 4*Xw*(Xw+15*Yw+3*Zw);
  double Vw_ = 9*Yw*(Xw+15*Yw+3*Zw);
  double U_ = U/(13*L) + Uw_;
  double V_ = V/(13*L) + Vw_;
  *ret_Y = pow (laB_XYZ_F ((L+16) / 116), -1) * Yw;
  *ret_X = -(9 * (*ret_Y) * U_)/((U_ - 4)*V_ - U_*V_);
  *ret_Z = (9 * (*ret_Y) - (15 * V_ * (*ret_Y)) - (V_ * (*ret_X))) / (3 * V_);
}
