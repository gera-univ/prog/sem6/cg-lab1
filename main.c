#include <gtk/gtk.h>
#include "conversions.h"
#include "ranges.h"

GtkWidget *color_button;

GtkWidget *r_scale;
GtkWidget *g_scale;
GtkWidget *b_scale;

GtkWidget *c_scale;
GtkWidget *m_scale;
GtkWidget *y_scale;
GtkWidget *k_scale;

GtkWidget *h_scale;
GtkWidget *s_scale;
GtkWidget *v_scale;

GtkWidget *X_scale;
GtkWidget *Y_scale;
GtkWidget *Z_scale;

GtkWidget *l_scale;
GtkWidget *a_scale;
GtkWidget *B_scale;

GtkWidget *L_scale;
GtkWidget *U_scale;
GtkWidget *V_scale;

GtkWidget *r_entry;
GtkWidget *g_entry;
GtkWidget *b_entry;

GtkWidget *c_entry;
GtkWidget *m_entry;
GtkWidget *y_entry;
GtkWidget *k_entry;

GtkWidget *h_entry;
GtkWidget *s_entry;
GtkWidget *v_entry;

GtkWidget *X_entry;
GtkWidget *Y_entry;
GtkWidget *Z_entry;

GtkWidget *l_entry;
GtkWidget *a_entry;
GtkWidget *B_entry;

GtkWidget *L_entry;
GtkWidget *U_entry;
GtkWidget *V_entry;

GtkWidget *rgb_incorrect_label;
GtkWidget *cmyk_incorrect_label;
GtkWidget *hsv_incorrect_label;
GtkWidget *XYZ_incorrect_label;
GtkWidget *laB_incorrect_label;
GtkWidget *LUV_incorrect_label;

static gulong
get_signal_handler_id (GtkWidget *widget, GType type, const char *signal_name)
{
  guint signal_id = g_signal_lookup (signal_name,
                                     type);
  gulong handler_id = g_signal_handler_find ((GObject *) widget,
    G_SIGNAL_MATCH_ID, signal_id, 0, NULL, NULL, NULL);
  return handler_id;
}

static void
block_widget_signal (GtkWidget *widget, GType type, const char *signal_name)
{
  gulong handler_id = get_signal_handler_id (widget, type, signal_name);
  g_signal_handler_block ((GObject *) widget, handler_id);
}

static void
unblock_widget_signal (GtkWidget *widget, GType type, const char *signal_name)
{
  gulong handler_id = get_signal_handler_id (widget, type, signal_name);
  g_signal_handler_unblock ((GObject *) widget, handler_id);
}

static void
update_entries (GtkWidget *widget, double red, double green, double blue)
{
  // block 'changed' signal handlers to prevent event loop
  block_widget_signal (r_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (g_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (b_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (c_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (m_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (y_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (k_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (h_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (s_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (v_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (X_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (Y_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (Z_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (l_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (a_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (B_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (L_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (U_entry, GTK_TYPE_EDITABLE, "changed");
  block_widget_signal (V_entry, GTK_TYPE_EDITABLE, "changed");

  char text[50];
  snprintf (text, 50, "%lf", red);
  if (r_entry != widget)
    gtk_editable_set_text ((GtkEditable *) r_entry, text);
  snprintf (text, 50, "%lf", green);
  if (g_entry != widget)
    gtk_editable_set_text ((GtkEditable *) g_entry, text);
  snprintf (text, 50, "%lf", blue);
  if (b_entry != widget)
    gtk_editable_set_text ((GtkEditable *) b_entry, text);
  gtk_widget_set_visible (rgb_incorrect_label,
                          !check_rgb_range (red, green, blue));

  double cyan, magenta, yellow, key;
  convert_rgb_to_cmyk (red, green, blue, &cyan, &magenta, &yellow, &key);
  snprintf (text, 50, "%lf", cyan);
  if (c_entry != widget)
    gtk_editable_set_text ((GtkEditable *) c_entry, text);
  snprintf (text, 50, "%lf", magenta);
  if (m_entry != widget)
    gtk_editable_set_text ((GtkEditable *) m_entry, text);
  snprintf (text, 50, "%lf", yellow);
  if (y_entry != widget)
    gtk_editable_set_text ((GtkEditable *) y_entry, text);
  snprintf (text, 50, "%lf", key);
  if (k_entry != widget)
    gtk_editable_set_text ((GtkEditable *) k_entry, text);
  gtk_widget_set_visible (cmyk_incorrect_label,
                          !check_cmyk_range (cyan, magenta, yellow, key));

  double hue, saturation, value;
  convert_rgb_to_hsv (red, green, blue, &hue, &saturation, &value);
  snprintf (text, 50, "%lf", hue);
  if (h_entry != widget)
    gtk_editable_set_text ((GtkEditable *) h_entry, text);
  snprintf (text, 50, "%lf", saturation);
  if (s_entry != widget)
    gtk_editable_set_text ((GtkEditable *) s_entry, text);
  snprintf (text, 50, "%lf", value);
  if (v_entry != widget)
    gtk_editable_set_text ((GtkEditable *) v_entry, text);
  gtk_widget_set_visible (hsv_incorrect_label,
                          !check_hsv_range (hue, saturation, value));

  double X, Y, Z;
  convert_rgb_to_XYZ (red, green, blue, &X, &Y, &Z);
  snprintf (text, 50, "%lf", X);
  if (X_entry != widget)
    gtk_editable_set_text ((GtkEditable *) X_entry, text);
  snprintf (text, 50, "%lf", Y);
  if (Y_entry != widget)
    gtk_editable_set_text ((GtkEditable *) Y_entry, text);
  snprintf (text, 50, "%lf", Z);
  if (Z_entry != widget)
    gtk_editable_set_text ((GtkEditable *) Z_entry, text);
  gtk_widget_set_visible (XYZ_incorrect_label, !check_XYZ_range(X, Y, Z));

  double l, a, B;
  convert_XYZ_to_laB (X, Y, Z, &l, &a, &B);
  snprintf (text, 50, "%lf", l);
  if (l_entry != widget)
    gtk_editable_set_text ((GtkEditable *) l_entry, text);
  snprintf (text, 50, "%lf", a);
  if (a_entry != widget)
    gtk_editable_set_text ((GtkEditable *) a_entry, text);
  snprintf (text, 50, "%lf", B);
  if (B_entry != widget)
    gtk_editable_set_text ((GtkEditable *) B_entry, text);
  gtk_widget_set_visible (laB_incorrect_label, !check_laB_range(l, a, B));

  double L, U, V;
  convert_XYZ_to_LUV (X, Y, Z, &L, &U, &V);
  snprintf (text, 50, "%lf", L);
  if (L_entry != widget)
    gtk_editable_set_text ((GtkEditable *) L_entry, text);
  snprintf (text, 50, "%lf", U);
  if (U_entry != widget)
    gtk_editable_set_text ((GtkEditable *) U_entry, text);
  snprintf (text, 50, "%lf", V);
  if (V_entry != widget)
    gtk_editable_set_text ((GtkEditable *) V_entry, text);
  gtk_widget_set_visible (LUV_incorrect_label, !check_LUV_range(L, U, V));

  unblock_widget_signal (r_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (g_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (b_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (c_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (m_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (y_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (k_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (h_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (s_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (v_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (X_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (Y_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (Z_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (l_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (a_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (B_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (L_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (U_entry, GTK_TYPE_EDITABLE, "changed");
  unblock_widget_signal (V_entry, GTK_TYPE_EDITABLE, "changed");
}

static void
update_all_values (GtkWidget *widget, double red, double green, double blue)
{
  // update sliders
  block_widget_signal (r_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (g_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (b_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (c_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (m_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (y_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (k_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (h_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (s_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (v_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (X_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (Y_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (Z_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (l_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (a_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (B_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (L_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (U_scale, GTK_TYPE_RANGE, "value-changed");
  block_widget_signal (V_scale, GTK_TYPE_RANGE, "value-changed");
  if (widget != r_scale) gtk_range_set_value ((GtkRange *) r_scale, red);
  if (widget != g_scale) gtk_range_set_value ((GtkRange *) g_scale, green);
  if (widget != b_scale) gtk_range_set_value ((GtkRange *) b_scale, blue);
  double cyan, magenta, yellow, key;
  convert_rgb_to_cmyk (red, green, blue, &cyan, &magenta, &yellow, &key);
  if (widget != c_scale) gtk_range_set_value ((GtkRange *) c_scale, cyan);
  if (widget != m_scale) gtk_range_set_value ((GtkRange *) m_scale, magenta);
  if (widget != y_scale) gtk_range_set_value ((GtkRange *) y_scale, yellow);
  if (widget != k_scale) gtk_range_set_value ((GtkRange *) k_scale, key);
  double hue, saturation, value;
  convert_rgb_to_hsv(red, green, blue, &hue, &saturation, &value);
  if (widget != h_scale) gtk_range_set_value ((GtkRange *) h_scale, hue);
  if (widget != s_scale) gtk_range_set_value ((GtkRange *) s_scale, saturation);
  if (widget != v_scale) gtk_range_set_value ((GtkRange *) v_scale, value);
  double X, Y, Z;
  convert_rgb_to_XYZ(red, green, blue, &X, &Y, &Z);
  if (widget != X_scale) gtk_range_set_value ((GtkRange *) X_scale, X);
  if (widget != Y_scale) gtk_range_set_value ((GtkRange *) Y_scale, Y);
  if (widget != Z_scale) gtk_range_set_value ((GtkRange *) Z_scale, Z);
  double l, a, B;
  convert_XYZ_to_laB(X, Y, Z, &l, &a, &B);
  if (widget != l_scale) gtk_range_set_value ((GtkRange *) l_scale, l);
  if (widget != a_scale) gtk_range_set_value ((GtkRange *) a_scale, a);
  if (widget != B_scale) gtk_range_set_value ((GtkRange *) B_scale, B);
  double L, U, V;
  convert_XYZ_to_LUV (X, Y, Z, &L, &U, &V);
  if (widget != L_scale) gtk_range_set_value ((GtkRange *) L_scale, L);
  if (widget != U_scale) gtk_range_set_value ((GtkRange *) U_scale, U);
  if (widget != V_scale) gtk_range_set_value ((GtkRange *) V_scale, V);
  unblock_widget_signal (r_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (g_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (b_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (c_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (m_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (y_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (k_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (h_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (s_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (v_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (X_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (Y_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (Z_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (l_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (a_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (B_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (L_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (U_scale, GTK_TYPE_RANGE, "value-changed");
  unblock_widget_signal (V_scale, GTK_TYPE_RANGE, "value-changed");

  // update entries
  update_entries(widget, red, green, blue);

  // update color chooser; get alpha value from previous color
  GdkRGBA old_color;
  gtk_color_chooser_get_rgba ((GtkColorChooser *) color_button, &old_color);
  gtk_color_chooser_set_rgba ((GtkColorChooser *) color_button, 
  &(GdkRGBA) {
    .red = red / 255.0,
    .green = green / 255.0,
    .blue = blue / 255.0,
    .alpha = old_color.alpha
  });
}

static void
on_rgb_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double red = gtk_range_get_value ((GtkRange *) r_scale);
  double green = gtk_range_get_value ((GtkRange *) g_scale);
  double blue = gtk_range_get_value ((GtkRange *) b_scale);
  update_all_values (widget, red, green, blue);
}

static void
on_cmyk_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double cyan = gtk_range_get_value ((GtkRange *) c_scale);
  double magenta = gtk_range_get_value ((GtkRange *) m_scale);
  double yellow = gtk_range_get_value ((GtkRange *) y_scale);
  double key = gtk_range_get_value ((GtkRange *) k_scale);
  double red, green, blue;
  convert_cmyk_to_rgb (cyan, magenta, yellow, key, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_hsv_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double hue = gtk_range_get_value ((GtkRange *) h_scale);
  double saturation = gtk_range_get_value ((GtkRange *) s_scale);
  double value = gtk_range_get_value ((GtkRange *) v_scale);
  double red, green, blue;
  convert_hsv_to_rgb (hue, saturation, value, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_XYZ_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double X = gtk_range_get_value ((GtkRange *) X_scale);
  double Y = gtk_range_get_value ((GtkRange *) Y_scale);
  double Z = gtk_range_get_value ((GtkRange *) Z_scale);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_laB_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double l = gtk_range_get_value ((GtkRange *) l_scale);
  double a = gtk_range_get_value ((GtkRange *) a_scale);
  double B = gtk_range_get_value ((GtkRange *) B_scale);
  double X, Y, Z;
  convert_laB_to_XYZ (l, a, B, &X, &Y, &Z);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_LUV_scale_value_changed (GtkWidget *widget,
                            gpointer   data)
{
  double L = gtk_range_get_value ((GtkRange *) L_scale);
  double U = gtk_range_get_value ((GtkRange *) U_scale);
  double V = gtk_range_get_value ((GtkRange *) V_scale);
  double X, Y, Z;
  convert_LUV_to_XYZ (L, U, V, &X, &Y, &Z);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_rgb_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *r_entry_text = gtk_editable_get_text ((GtkEditable *) r_entry);
  const char *g_entry_text = gtk_editable_get_text ((GtkEditable *) g_entry);
  const char *b_entry_text = gtk_editable_get_text ((GtkEditable *) b_entry);
  double red = CLAMP (strtod (r_entry_text, NULL), 0, 255);
  double green = CLAMP (strtod (g_entry_text, NULL), 0, 255);
  double blue = CLAMP (strtod (b_entry_text, NULL), 0, 255);
  update_all_values (widget, red, green, blue);
}

static void
on_cmyk_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *c_entry_text = gtk_editable_get_text ((GtkEditable *) c_entry);
  const char *m_entry_text = gtk_editable_get_text ((GtkEditable *) m_entry);
  const char *y_entry_text = gtk_editable_get_text ((GtkEditable *) y_entry);
  const char *k_entry_text = gtk_editable_get_text ((GtkEditable *) k_entry);
  double cyan = CLAMP (strtod (c_entry_text, NULL), 0.0, 1.0);
  double magenta = CLAMP (strtod (m_entry_text, NULL), 0.0, 1.0);
  double yellow = CLAMP (strtod (y_entry_text, NULL), 0.0, 1.0);
  double key = CLAMP (strtod (k_entry_text, NULL), 0.0, 1.0);
  double red, green, blue;
  convert_cmyk_to_rgb (cyan, magenta, yellow, key, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_hsv_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *h_entry_text = gtk_editable_get_text ((GtkEditable *) h_entry);
  const char *s_entry_text = gtk_editable_get_text ((GtkEditable *) s_entry);
  const char *v_entry_text = gtk_editable_get_text ((GtkEditable *) v_entry);
  double hue = CLAMP (strtod (h_entry_text, NULL), 0.0, 360.0);
  double saturation = CLAMP (strtod (s_entry_text, NULL), 0.0, 1.0);
  double value = CLAMP (strtod (v_entry_text, NULL), 0.0, 1.0);
  double red, green, blue;
  convert_hsv_to_rgb (hue, saturation, value, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_XYZ_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *X_entry_text = gtk_editable_get_text ((GtkEditable *) X_entry);
  const char *Y_entry_text = gtk_editable_get_text ((GtkEditable *) Y_entry);
  const char *Z_entry_text = gtk_editable_get_text ((GtkEditable *) Z_entry);
  double X = CLAMP (strtod (X_entry_text, NULL), 0.0, 100.0);
  double Y = CLAMP (strtod (Y_entry_text, NULL), 0.0, 100.0);
  double Z = CLAMP (strtod (Z_entry_text, NULL), 0.0, 100.0);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_laB_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *l_entry_text = gtk_editable_get_text ((GtkEditable *) l_entry);
  const char *a_entry_text = gtk_editable_get_text ((GtkEditable *) a_entry);
  const char *B_entry_text = gtk_editable_get_text ((GtkEditable *) B_entry);
  double l = CLAMP (strtod (l_entry_text, NULL), 0.0, 100.0);
  double a = CLAMP (strtod (a_entry_text, NULL), -128.0, 127.0);
  double B = CLAMP (strtod (B_entry_text, NULL), -128.0, 127.0);
  double X, Y, Z;
  convert_laB_to_XYZ (l, a, B, &X, &Y, &Z);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_LUV_entry_changed (GtkWidget *widget,
                      gpointer   data)
{
  const char *L_entry_text = gtk_editable_get_text ((GtkEditable *) L_entry);
  const char *U_entry_text = gtk_editable_get_text ((GtkEditable *) U_entry);
  const char *V_entry_text = gtk_editable_get_text ((GtkEditable *) V_entry);
  double L = CLAMP (strtod (L_entry_text, NULL), 0.0, 100.0);
  double U = CLAMP (strtod (U_entry_text, NULL), -100.0, 100.0);
  double V = CLAMP (strtod (V_entry_text, NULL), -100.0, 100.0);
  double X, Y, Z;
  convert_laB_to_XYZ (L, U, V, &X, &Y, &Z);
  double red, green, blue;
  convert_XYZ_to_rgb (X, Y, Z, &red, &green, &blue);
  update_all_values (widget, red, green, blue);
}

static void
on_color_set (GtkColorChooser *chooser,
              gpointer         data)
{
  GdkRGBA color;
  gtk_color_chooser_get_rgba ((GtkColorChooser *) color_button, &color);
  update_all_values (NULL,
                    color.red * 255.0,
                    color.green * 255.0,
                    color.blue * 255.0);
}

static GtkWidget * 
values_incorrect_label_new ()
{
  GtkWidget *label = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (label),
                       "<span foreground='red'>Values are incorrect!</span>");
  gtk_widget_set_visible(label, false);
  return label;
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *grid;
  GtkWidget *r_label;
  GtkWidget *g_label;
  GtkWidget *b_label;
  GtkWidget *c_label;
  GtkWidget *m_label;
  GtkWidget *y_label;
  GtkWidget *k_label;
  GtkWidget *h_label;
  GtkWidget *s_label;
  GtkWidget *v_label;
  GtkWidget *X_label;
  GtkWidget *Y_label;
  GtkWidget *Z_label;
  GtkWidget *l_label;
  GtkWidget *a_label;
  GtkWidget *B_label;
  GtkWidget *L_label;
  GtkWidget *U_label;
  GtkWidget *V_label;
  GtkWidget *separator;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "cg-lab1");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

  grid = gtk_grid_new ();
  gtk_window_set_child (GTK_WINDOW (window), grid);

  int row = 0;
  
  // ---------- rgb ----------
  r_label = gtk_label_new ("R");
  g_label = gtk_label_new ("G");
  b_label = gtk_label_new ("B");
  rgb_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), r_label, 0, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), g_label, 0, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), b_label, 0, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), rgb_incorrect_label,
                                             1, row+4, 2, 1);

  r_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 255.0, 1.0);
  g_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 255.0, 1.0);
  b_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 255.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), r_scale, 1, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), g_scale, 1, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), b_scale, 1, row+3, 1, 1);
  gtk_widget_set_hexpand (r_scale, true);
  gtk_widget_set_hexpand (g_scale, true);
  gtk_widget_set_hexpand (b_scale, true);

  r_entry = gtk_entry_new (); 
  g_entry = gtk_entry_new (); 
  b_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), r_entry, 2, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), g_entry, 2, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), b_entry, 2, row+3, 1, 1);


  separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 0, row+5, 3, 1);

  row += 5;

  // ---------- cmyk ----------
  c_label = gtk_label_new ("C");
  m_label = gtk_label_new ("M");
  y_label = gtk_label_new ("Y");
  k_label = gtk_label_new ("K");
  cmyk_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), c_label, 0, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), m_label, 0, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), y_label, 0, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), k_label, 0, row+4, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), cmyk_incorrect_label,
                                             1, row+5, 2, 1);

  c_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  m_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  y_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  k_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), c_scale, 1, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), m_scale, 1, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), y_scale, 1, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), k_scale, 1, row+4, 1, 1);
  gtk_widget_set_hexpand (c_scale, true);
  gtk_widget_set_hexpand (m_scale, true);
  gtk_widget_set_hexpand (y_scale, true);
  gtk_widget_set_hexpand (k_scale, true);
  
  c_entry = gtk_entry_new (); 
  m_entry = gtk_entry_new (); 
  y_entry = gtk_entry_new (); 
  k_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), c_entry, 2, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), m_entry, 2, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), y_entry, 2, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), k_entry, 2, row+4, 1, 1);

  separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 0, row+6, 3, 1);

  row += 6;

  // ---------- hsv ----------
  h_label = gtk_label_new ("H");
  s_label = gtk_label_new ("S");
  v_label = gtk_label_new ("V");
  hsv_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), h_label, 0, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), s_label, 0, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), v_label, 0, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), hsv_incorrect_label,
                                             1, row+4, 2, 1);

  h_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 360.0, 1.0);
  s_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  v_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 1.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), h_scale, 1, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), s_scale, 1, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), v_scale, 1, row+3, 1, 1);

  h_entry = gtk_entry_new (); 
  s_entry = gtk_entry_new (); 
  v_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), h_entry, 2, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), s_entry, 2, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), v_entry, 2, row+3, 1, 1);

  row = 0;

  // ---------- XYZ ----------
  separator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 3, 1, 1, 5);

  X_label = gtk_label_new ("X");
  Y_label = gtk_label_new ("Y");
  Z_label = gtk_label_new ("Z");
  XYZ_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), X_label, 4, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Y_label, 4, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Z_label, 4, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), XYZ_incorrect_label,
                                             5, row+4, 2, 1);

  X_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 100.0, 1.0);
  Y_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 100.0, 1.0);
  Z_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 100.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), X_scale, 5, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Y_scale, 5, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Z_scale, 5, row+3, 1, 1);
  gtk_widget_set_hexpand (X_scale, true);
  gtk_widget_set_hexpand (Y_scale, true);
  gtk_widget_set_hexpand (Z_scale, true);

  X_entry = gtk_entry_new (); 
  Y_entry = gtk_entry_new (); 
  Z_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), X_entry, 6, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Y_entry, 6, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), Z_entry, 6, row+3, 1, 1);

  separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 4, row+5, 3, 1);

  row += 5;

  // ---------- laB ----------
  separator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 3, 1, 1, 5);

  l_label = gtk_label_new ("L");
  a_label = gtk_label_new ("A");
  B_label = gtk_label_new ("B");
  laB_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), l_label, 4, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), a_label, 4, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), B_label, 4, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), laB_incorrect_label,
                                             5, row+4, 2, 1);

  l_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 100.0, 1.0);
  a_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      -128.0, 127.0, 1.0);
  B_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      -128.0, 127.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), l_scale, 5, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), a_scale, 5, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), B_scale, 5, row+3, 1, 1);
  gtk_widget_set_hexpand (l_scale, true);
  gtk_widget_set_hexpand (a_scale, true);
  gtk_widget_set_hexpand (B_scale, true);

  l_entry = gtk_entry_new (); 
  a_entry = gtk_entry_new (); 
  B_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), l_entry, 6, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), a_entry, 6, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), B_entry, 6, row+3, 1, 1);

  separator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 4, row+5, 3, 1);

  row += 5; 

  // ---------- LUV ----------
  separator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);
  gtk_grid_attach (GTK_GRID (grid), separator, 3, 1, 1, 5);

  L_label = gtk_label_new ("L");
  U_label = gtk_label_new ("U");
  V_label = gtk_label_new ("V");
  LUV_incorrect_label = values_incorrect_label_new ();
  gtk_grid_attach (GTK_GRID (grid), L_label, 4, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), U_label, 4, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), V_label, 4, row+3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), LUV_incorrect_label,
                                             5, row+4, 2, 1);

  L_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      0.0, 100.0, 1.0);
  U_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      -100.0, 100.0, 1.0);
  V_scale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL,
                                      -100.0, 100.0, 1.0);
  gtk_grid_attach (GTK_GRID (grid), L_scale, 5, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), U_scale, 5, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), V_scale, 5, row+3, 1, 1);
  gtk_widget_set_hexpand (L_scale, true);
  gtk_widget_set_hexpand (U_scale, true);
  gtk_widget_set_hexpand (V_scale, true);

  L_entry = gtk_entry_new (); 
  U_entry = gtk_entry_new (); 
  V_entry = gtk_entry_new (); 
  gtk_grid_attach (GTK_GRID (grid), L_entry, 6, row+1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), U_entry, 6, row+2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), V_entry, 6, row+3, 1, 1);

  // ----------

  color_button = gtk_color_button_new ();
  gtk_grid_attach (GTK_GRID (grid), color_button, 0, 0, 7, 1);
  gtk_widget_set_vexpand (color_button, true);

  g_signal_connect (r_scale, "value-changed",
                    G_CALLBACK (on_rgb_scale_value_changed), NULL);
  g_signal_connect (g_scale, "value-changed",
                    G_CALLBACK (on_rgb_scale_value_changed), NULL);
  g_signal_connect (b_scale, "value-changed",
                    G_CALLBACK (on_rgb_scale_value_changed), NULL);
  g_signal_connect (c_scale, "value-changed",
                    G_CALLBACK (on_cmyk_scale_value_changed), NULL);
  g_signal_connect (m_scale, "value-changed",
                    G_CALLBACK (on_cmyk_scale_value_changed), NULL);
  g_signal_connect (y_scale, "value-changed",
                    G_CALLBACK (on_cmyk_scale_value_changed), NULL);
  g_signal_connect (k_scale, "value-changed",
                    G_CALLBACK (on_cmyk_scale_value_changed), NULL);
  g_signal_connect (h_scale, "value-changed",
                    G_CALLBACK (on_hsv_scale_value_changed), NULL);
  g_signal_connect (s_scale, "value-changed",
                    G_CALLBACK (on_hsv_scale_value_changed), NULL);
  g_signal_connect (v_scale, "value-changed",
                    G_CALLBACK (on_hsv_scale_value_changed), NULL);
  g_signal_connect (X_scale, "value-changed",
                    G_CALLBACK (on_XYZ_scale_value_changed), NULL);
  g_signal_connect (Y_scale, "value-changed",
                    G_CALLBACK (on_XYZ_scale_value_changed), NULL);
  g_signal_connect (Z_scale, "value-changed",
                    G_CALLBACK (on_XYZ_scale_value_changed), NULL);
  g_signal_connect (l_scale, "value-changed",
                    G_CALLBACK (on_laB_scale_value_changed), NULL);
  g_signal_connect (a_scale, "value-changed",
                    G_CALLBACK (on_laB_scale_value_changed), NULL);
  g_signal_connect (B_scale, "value-changed",
                    G_CALLBACK (on_laB_scale_value_changed), NULL);
  g_signal_connect (L_scale, "value-changed",
                    G_CALLBACK (on_LUV_scale_value_changed), NULL);
  g_signal_connect (U_scale, "value-changed",
                    G_CALLBACK (on_LUV_scale_value_changed), NULL);
  g_signal_connect (V_scale, "value-changed",
                    G_CALLBACK (on_LUV_scale_value_changed), NULL);
  g_signal_connect (r_entry, "changed",
                    G_CALLBACK (on_rgb_entry_changed), NULL);
  g_signal_connect (g_entry, "changed",
                    G_CALLBACK (on_rgb_entry_changed), NULL);
  g_signal_connect (b_entry, "changed",
                    G_CALLBACK (on_rgb_entry_changed), NULL);
  g_signal_connect (c_entry, "changed",
                    G_CALLBACK (on_cmyk_entry_changed), NULL);
  g_signal_connect (m_entry, "changed",
                    G_CALLBACK (on_cmyk_entry_changed), NULL);
  g_signal_connect (y_entry, "changed",
                    G_CALLBACK (on_cmyk_entry_changed), NULL);
  g_signal_connect (k_entry, "changed",
                    G_CALLBACK (on_cmyk_entry_changed), NULL);
  g_signal_connect (h_entry, "changed",
                    G_CALLBACK (on_hsv_entry_changed), NULL);
  g_signal_connect (s_entry, "changed",
                    G_CALLBACK (on_hsv_entry_changed), NULL);
  g_signal_connect (v_entry, "changed",
                    G_CALLBACK (on_hsv_entry_changed), NULL);
  g_signal_connect (X_entry, "changed",
                    G_CALLBACK (on_XYZ_entry_changed), NULL);
  g_signal_connect (Y_entry, "changed",
                    G_CALLBACK (on_XYZ_entry_changed), NULL);
  g_signal_connect (Z_entry, "changed",
                    G_CALLBACK (on_XYZ_entry_changed), NULL);
  g_signal_connect (l_entry, "changed",
                    G_CALLBACK (on_laB_entry_changed), NULL);
  g_signal_connect (a_entry, "changed",
                    G_CALLBACK (on_laB_entry_changed), NULL);
  g_signal_connect (B_entry, "changed",
                    G_CALLBACK (on_laB_entry_changed), NULL);
  g_signal_connect (L_entry, "changed",
                    G_CALLBACK (on_LUV_entry_changed), NULL);
  g_signal_connect (U_entry, "changed",
                    G_CALLBACK (on_LUV_entry_changed), NULL);
  g_signal_connect (V_entry, "changed",
                    G_CALLBACK (on_LUV_entry_changed), NULL);
  g_signal_connect (color_button, "color-set",
                    G_CALLBACK (on_color_set), NULL);

  gtk_widget_show (window); 
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;

  int status;

  app = gtk_application_new ("org.spiralarms.cg-lab1",
                             G_APPLICATION_FLAGS_NONE);

  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
